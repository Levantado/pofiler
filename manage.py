from app import create_app
from flask.cli import FlaskGroup
from app.utils.dbs import db
from app.api.users.models import User


cli = FlaskGroup(create_app())


@cli.command('recreate_db')
def recreate_db():
    db.drop_all()
    db.create_all()
    db.session.commit()

@cli.command('seed_db')
def seed_db():
    db.session.add(User(username='test1', email="test1@gmail.com"))
    db.session.add(User(username='test2', email="test2@gmail.com"))
    db.session.commit()

if __name__ == '__main__':
    cli()
