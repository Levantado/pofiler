import os

from flask_restplus import Api

from app.api.ping import ping_namespace
from app.api.users.views import users_namespace

if os.getenv("FLASK_ENV") == "development":
    api = Api(version="1.0", title="Users API", prefix="/api", doc="/doc/")
else:
    api = Api(version="1.0", title="Users API", prefix="/api")

api.add_namespace(ping_namespace, path="/ping")
api.add_namespace(users_namespace, path="/users")
