from app.utils.dbs import db
from sqlalchemy.sql import func
import os


class User(db.Model):
    __tablename__ = "users"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(128), unique=True, nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    active = db.Column(db.Boolean(), default=True, nullable=False)
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, username, email):
        self.username = username
        self.email = email


if os.getenv("FLASK_ENV") == "development":
    from app.utils.adm import admin
    from app.api.users.admin import UsersAdminView

    admin.add_view(UsersAdminView(User, db.session))
