from flask import request
from flask_restplus import Resource, fields, Namespace

from app.api.users.services import (
    get_all_users,
    get_user_by_email,
    add_user,
    get_user_by_id,
    update_user,
    delete_user,
)


users_namespace = Namespace("users")

user = users_namespace.model(
    "User",
    {
        "id": fields.Integer(readOnly=True),
        "username": fields.String(required=True),
        "email": fields.String(required=True),
        "created_date": fields.DateTime,
    },
)


class Users(Resource):
    @users_namespace.marshal_with(user)
    @users_namespace.response(200, "Success")
    @users_namespace.response(404, "User <user_id> does not exist")
    def get(self, user_id):
        """Returns a single user."""
        userf = get_user_by_id(user_id)
        if not userf:
            users_namespace.abort(404, f"User {user_id} does not exist")
        return userf, 200

    @users_namespace.response(200, "<user_id> was removed!")
    @users_namespace.response(404, "User <user_id> does not exist")
    def delete(self, user_id):
        """Delete a user."""
        response_object = {}
        userf = get_user_by_id(user_id)
        if not userf:
            users_namespace.abort(404, f"User {user_id} does not exist")
        delete_user(userf)
        response_object["message"] = f"{userf.email} was removed!"
        return response_object, 200

    @users_namespace.expect(user, validate=True)
    @users_namespace.response(200, "<user_id> was updated!")
    @users_namespace.response(404, "User <user_id> does not exist")
    def put(self, user_id):
        """Update a user."""
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}

        userf = get_user_by_id(user_id)
        if not userf:
            users_namespace.abort(404, f"User {user_id} does not exist")
        update_user(userf, username, email)
        response_object["message"] = f"{userf.id} was updated!"
        return response_object, 200


class UsersList(Resource):
    @users_namespace.marshal_list_with(user)
    def get(self):
        """Returns all users."""
        return get_all_users(), 200

    @users_namespace.expect(user, validate=True)
    @users_namespace.response(201, "<user_email> was added!")
    @users_namespace.response(400, "Sorry. That email already exists.")
    def post(self):
        """Creates a new user."""
        post_data = request.get_json()
        username = post_data.get("username")
        email = post_data.get("email")
        response_object = {}
        userf = get_user_by_email(email)
        if userf:
            response_object["message"] = "Sorry. That email already exists."
            return response_object, 400
        add_user(username, email)
        response_object = {"status": "success", "message": f"{email} was added!"}
        return response_object, 201


users_namespace.add_resource(UsersList, "")
users_namespace.add_resource(Users, "/<int:user_id>")
