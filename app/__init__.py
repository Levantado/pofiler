import os
from app.utils.dbs import migrate, db
from app.utils.adm import admin
from flask import Flask


app_settings = os.getenv("APP_SETTINGS", "app.config.DevelopmentConfig")


def create_app():
    app = Flask(__name__)
    app.config.from_object(app_settings)
    db.app = app
    db.init_app(app)
    migrate.init_app(app, db)
    if os.getenv("FLASK_ENV") == "development":
        admin.init_app(app)

    from app.api import api

    api.init_app(app)

    @app.shell_context_processor
    def ctx():
        return {"app": app, "db": db}

    return app
